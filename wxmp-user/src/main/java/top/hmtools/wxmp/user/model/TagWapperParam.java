package top.hmtools.wxmp.user.model;

public class TagWapperParam {

	private TagParam tag;

	public TagParam getTag() {
		return tag;
	}

	public void setTag(TagParam tag) {
		this.tag = tag;
	}

	@Override
	public String toString() {
		return "TagWapperParam [tag=" + tag + "]";
	}
	
	
}
