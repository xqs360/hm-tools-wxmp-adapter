package top.hmtools.wxmp.user.model;

public class UserListParam {

	private String next_openid;

	public String getNext_openid() {
		return next_openid;
	}

	public void setNext_openid(String next_openid) {
		this.next_openid = next_openid;
	}
	
	
}
