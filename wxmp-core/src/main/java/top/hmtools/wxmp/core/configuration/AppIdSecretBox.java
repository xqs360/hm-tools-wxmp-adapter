package top.hmtools.wxmp.core.configuration;

/**
 * 存放微信公众号appid与appsecret数据对的盒子
 * @author HyboWork
 *
 */
public interface AppIdSecretBox {

	/**
	 * 获取微信公众号appid与appsecret数据对
	 * @return
	 */
	public AppIdSecretPair getAppIdSecretPair();
}
