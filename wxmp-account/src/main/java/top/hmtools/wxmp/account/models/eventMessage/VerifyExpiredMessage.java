package top.hmtools.wxmp.account.models.eventMessage;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseEventMessage;
import top.hmtools.wxmp.core.model.message.enums.Event;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 认证过期失效通知审通知
 * {@code
 * <xml>
  <ToUserName><![CDATA[toUser]]></ToUserName>  
  <FromUserName><![CDATA[fromUser]]></FromUserName>  
  <CreateTime>1442400900</CreateTime>  
  <MsgType><![CDATA[event]]></MsgType>  
  <Event><![CDATA[verify_expired]]></Event>  
  <ExpiredTime>1442400900</ExpiredTime> 
</xml>
 * }
 * @author HyboWork
 *
 */
@WxmpMessage(msgType=MsgType.event,event=Event.verify_expired)
public class VerifyExpiredMessage extends BaseEventMessage {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2912703476892179959L;
	
	@XStreamAlias("ExpiredTime")
	private Long expiredTime;
	
	

	public Long getExpiredTime() {
		return expiredTime;
	}



	public void setExpiredTime(Long expiredTime) {
		this.expiredTime = expiredTime;
	}



	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	@Override
	public String toString() {
		return "VerifyExpired [expiredTime=" + expiredTime + ", event=" + event + ", eventKey=" + eventKey
				+ ", toUserName=" + toUserName + ", fromUserName=" + fromUserName + ", createTime=" + createTime
				+ ", msgType=" + msgType + ", msgId=" + msgId + "]";
	}



	@Override
	public void configXStream(XStream xStream) {

	}

}
