package top.hmtools.wxmp.material.model;

import top.hmtools.wxmp.core.model.ErrcodeBean;

public class VideoResult extends ErrcodeBean{

	private String video_url;

	public String getVideo_url() {
		return video_url;
	}

	public void setVideo_url(String video_url) {
		this.video_url = video_url;
	}

	@Override
	public String toString() {
		return "VideoResult [video_url=" + video_url + ", errcode=" + errcode + ", errmsg=" + errmsg + "]";
	}
	
	
}
