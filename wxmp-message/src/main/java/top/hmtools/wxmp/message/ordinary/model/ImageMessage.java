package top.hmtools.wxmp.message.ordinary.model;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import top.hmtools.wxmp.core.annotation.WxmpMessage;
import top.hmtools.wxmp.core.model.message.BaseMessage;
import top.hmtools.wxmp.core.model.message.enums.MsgType;

/**
 * 图片消息
 * {@code
 * <xml>
	<ToUserName><![CDATA[toUser]]></ToUserName>
	<FromUserName><![CDATA[fromUser]]></FromUserName>
	<CreateTime>1348831860</CreateTime>
	<MsgType><![CDATA[image]]></MsgType>
	<PicUrl><![CDATA[this is a url]]></PicUrl>
	<MediaId><![CDATA[media_id]]></MediaId>
	<MsgId>1234567890123456</MsgId>
</xml>
 * }
 * @author Hybomyth
 *
 */
@WxmpMessage(msgType=MsgType.image)
public class ImageMessage extends BaseMessage{

	/**
	 * 
	 */
	private static final long serialVersionUID = -1215973474022741503L;

	/**
	 * 图片链接（由系统生成）
	 */
	@XStreamAlias("PicUrl")
	private String picUrl;
	
	/**
	 * 图片消息媒体id，可以调用多媒体文件下载接口拉取数据。
	 */
	@XStreamAlias("MediaId")
	private String mediaId;

	public String getPicUrl() {
		return picUrl;
	}

	public void setPicUrl(String picUrl) {
		this.picUrl = picUrl;
	}

	public String getMediaId() {
		return mediaId;
	}

	public void setMediaId(String mediaId) {
		this.mediaId = mediaId;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}


	@Override
	public void configXStream(XStream xStream) {
		
	}
	
	
}
