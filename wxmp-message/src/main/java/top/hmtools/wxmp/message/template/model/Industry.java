package top.hmtools.wxmp.message.template.model;

/**
 * Auto-generated: 2019-08-28 22:52:49
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class Industry {

	private String first_class;
	private String second_class;

	public void setFirst_class(String first_class) {
		this.first_class = first_class;
	}

	public String getFirst_class() {
		return first_class;
	}

	public void setSecond_class(String second_class) {
		this.second_class = second_class;
	}

	public String getSecond_class() {
		return second_class;
	}

	@Override
	public String toString() {
		return "Industry [first_class=" + first_class + ", second_class=" + second_class + "]";
	}

}