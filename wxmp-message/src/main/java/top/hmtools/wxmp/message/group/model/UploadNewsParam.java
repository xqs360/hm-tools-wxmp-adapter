package top.hmtools.wxmp.message.group.model;

import java.util.List;

/**
 * Auto-generated: 2019-08-26 9:52:46
 *
 * @author bejson.com (i@bejson.com)
 * @website http://www.bejson.com/java2pojo/
 */
public class UploadNewsParam {

	private List<Articles> articles;

	public void setArticles(List<Articles> articles) {
		this.articles = articles;
	}

	public List<Articles> getArticles() {
		return articles;
	}

}