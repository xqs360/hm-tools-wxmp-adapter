package top.hmtools.wxmp.message.group.model.openIdGroupSend;

/**
 * 消息管理--群发消息--根据openID群发--文本：
 * @author HyboWork
 *
 */
public class TextOpenIdGroupSendParam extends BaseOpenIdGroupSendParam {

	private Text text;

	public Text getText() {
		return text;
	}

	public void setText(Text text) {
		this.text = text;
	}

	@Override
	public String toString() {
		return "TextOpenIdGroupSendParam [text=" + text + ", msgtype=" + msgtype + ", touser=" + touser + "]";
	}
	
	
}
