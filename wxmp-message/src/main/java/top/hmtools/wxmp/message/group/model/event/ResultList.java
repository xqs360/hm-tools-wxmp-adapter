package top.hmtools.wxmp.message.group.model.event;

import java.util.List;

import com.thoughtworks.xstream.annotations.XStreamAlias;

public class ResultList {

	@XStreamAlias(value="item")
	private List<ArticleUrlResultItem> item;

	public List<ArticleUrlResultItem> getItem() {
		return item;
	}

	public void setItem(List<ArticleUrlResultItem> item) {
		this.item = item;
	}
	
	
}
