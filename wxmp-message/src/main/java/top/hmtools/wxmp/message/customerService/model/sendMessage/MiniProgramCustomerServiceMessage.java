package top.hmtools.wxmp.message.customerService.model.sendMessage;

import top.hmtools.wxmp.message.customerService.model.BaseSendMessageParam;

/**
 * 消息管理--客服消息--发送小程序卡片（要求小程序与公众号已关联）
 * @author HyboWork
 *
 */
public class MiniProgramCustomerServiceMessage extends BaseSendMessageParam{

	private Miniprogrampage miniprogrampage;

	public Miniprogrampage getMiniprogrampage() {
		return miniprogrampage;
	}

	public void setMiniprogrampage(Miniprogrampage miniprogrampage) {
		this.miniprogrampage = miniprogrampage;
	}

	@Override
	public String toString() {
		return "MiniProgramCustomerServiceMessage [miniprogrampage=" + miniprogrampage + ", touser=" + touser
				+ ", msgtype=" + msgtype + "]";
	}
	
	
}
